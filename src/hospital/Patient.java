package hospital;

import java.util.Date;
import java.util.*;

public class Patient {
    
    private String name;
    private Date date;
    private String familyDoctor;
    private final List<WristBands> bands;
    
    public Patient(String name, Date date, String familyDoctor, 
            List<WristBands> bands){
        
        setName(name);
        setDate(date);
        setFamilyDoctor(familyDoctor);
        this.bands = bands;
    }
    
    public List<WristBands> getTotalBands(){
       return bands;
    }
    
    public String getName(){
        return name;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public Date getDate(){
        return date;
    }
    
    public void setDate(Date date){
        this.date = date;
    }
    
    public String getFamilyDoctor(){
        return familyDoctor;
    }
    
    public void setFamilyDoctor(String familyDoctor){
        this.familyDoctor = familyDoctor;
    }
}
