package hospital;

import java.util.*;

public class ResearchGroup {

    private Date startTime;
    private Date endTime;
    private final List<Patient> patients;
    
    public ResearchGroup(List<Patient> patients){
        this.patients = patients;
    }
    
    public List<Patient> getTotalPatients(){
        return patients;
    }
    
    public Date getStartTime(){
        return startTime;
    }
    
    public void setStartTime(Date startTime){
        this.startTime = startTime; 
    }
    
    public Date getEndTime(){
        return endTime;
    }
    
    public void setEndTime(Date endTime){
        this.endTime = endTime; 
    }
   
    public long getTotalWaitTime(Date startTime, Date endTime){
        
        long waitPeriod = startTime.getTime() - endTime.getTime();
        
        return waitPeriod;
    }
}

